#!/bin/bash
: '
'
go_ver=go1.10.3
arch=$(uname -m)
echo $arch
if [ "$arch" == "x86_64" ]; then
    goarch=amd64
elif [ "$arch" == "armv7l" ] || [ "$arch" == "armv6l" ]; then
    goarch=armv6l
fi
echo $OSTYPE
if [ "$OSTYPE" == "linux-gnu" ] || [ "$OSTYPE" == "linux-gnueabihf" ] ; then
    echo 'linux'
else
    echo 'not linux "bye"'
    exit 2
fi
IFGO=$(which go)
echo $IFGO
if [ "$IFGO" == "/usr/local/go/bin/go" ] ; then
    echo "found go: $IFGO"
    GOVERSION=$(go version | cut -b1-19)
    if [ "$GOVERSION" == "go version $go_ver" ] ; then
        echo "$GOVERSION already installed"
        exit 0
    else
        echo "updating go to $go_ver"
    fi
else
    echo "Installing Golang"
    sudo echo 'export PATH=$PATH:/usr/local/go/bin' >> /etc/profile
fi
wget -c https://dl.google.com/go/$go_ver.linux-$goarch.tar.gz
sudo tar -C /usr/local -xzf $go_ver.linux-$goarch.tar.gz
rm $go_ver.linux-$goarch.tar.gz